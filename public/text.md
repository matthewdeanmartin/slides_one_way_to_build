# Goals

- Some mild advocacy.
Note: Build scripts are good and you should pay attention to them not existing or failing.

- Something you can use today.

# Problems you (may) have.

- You discover problems with your code late, possibly only when its in production.
- It takes forever to go from changing code to seeing it in production even after you ignore CAB and SDLC delays.

Build scripts find mistakes and automate most of the steps.


# Is it optional?


# Build Scripts are like governance

- Sometimes it is built in
  - you can't run code with bad datatypes in Java.
  - you can with python

- Code review is the most effective way to improve code quality
  - Linters do a better job of it than you
  - Code reformatters do an ever better job
  - Code review is now at a design level, not nitpick leve

# Build Scripts are like doing work

- If a build script was doing this work, someone would
- It is boring, repetitious work
- Skipping doing it doesn't always work well
- What do we want to skip, testing?
  Which errors are okay to leave in?

# Build Scripts are like a testing team

- Build scripts find bugs everywhere
- Build scripts drop the cycle time from discovery to fix


# Build Scripts Elements


# Typical High Value Tools

 - Type checker (sometimes called compilers)
 - Unit tests framework
 - Static code analysis
 - Dependency managers and packagers



# Getting there from here

- Legacy code
 - Expect transition period, fail build based on lines of lint
 - Write any unit tests, even bad ones are better than none
 - Add typing to python and typescript incrementally
 - Expect to invest some time

- Greenfield code
 - Start with a build script first
 - Fail build on more than a handful of issues reported
 - Start strict



# Solutions

build.sh
```bash
    pylint . && bandit . && mypy . && python setup.py sdist
```

makefile-like, e.g. maven, Makefile 

  - creates targets or output files when missing
  - targets have dependencies that need to be build once
  - skip if already build or if source didn't change

Dependency/Package centric

  - Pipfile
  - package.json
  - Dockerfile


# Build Servers

- Examples Gitlab's gitlab-ci.yml, Jenkinsfile (other exists such as circle ci, travis, github actions, Azure DevOps and many, many more)
- Gets work off your workstation
- Repeatablity
- Team visiblity into build status

```yaml
    build:
        script:
        - build.sh
```


# Challenge: Getting Started

- Creating the basic parts
	- Use shared templates!
	- CookieCutters for python projects

    
# Challenge: Running tools locally

	- No good solution!
	- All in one tools (mega linter)
	- Consider running tools in Docker (link)
    - Use precommit


# Challenge: Slow builds

Use a base image (link) 


# Challenge: Demonstrating to clients and management

Sonar
    - Mediocre workflow, doesn't replace a build script
    - Good for big picture reports 

Artifacts
    - HTML reports

# Deploying Packages

- Gitlab supports most package repositories
- AWS supports most package repositories too.


# Building Containers

- Docker in Docker for building
- Can "smoke test images"
- Gitlab docker repository
- AWS also has container repositories


# Continuous Deployment

- Spinning up machines
- Putting software on the machines to run
- Has become part of the build script now that "infrastructure is code"


# Provisioning AWS Infrastructure

- Use Terraform
- Spectacular complexity
- Unit of re-use is a module


# Deploy Scripts 

- Gitlab natively works with Kubernetes
- If you use ECs, Gitlab has fewer built in features
- Your terraform scripts will get intertwined with your deploy scripts

# Interactive Terraform for non-IAM resourced
- Interactive plan and apply for everything except IAM

plan.sh

```bash
# Are you even on the right branch?
# Don't pick the wrong credentials
export AWS_PROFILE=my-profile-name
# Are these credentials any good? TF checks late.
aws sts get-caller-identity
retVal=$?
if [ $retVal -ne 0 ]; then
   echo Current credentials are no good.
    kill -INT $$
    return
fi

CURRENT_BRANCH=$(git symbolic-ref --short -q HEAD)
if [ "$CURRENT_BRANCH" = "development" ]; then
    # make sure the origin reference is up to date
    # git fetch origin development
    #if $(git diff-index origin/development --quiet); then
      terraform workspace select dev
      terraform apply "plan.txt"
    #else
    #  echo "You need to push your changes to origin before applying changes"
    #fi
else
    echo "Wrong branch. Not applying"
fi
```

apply.sh

```bash
# Are you even on the right branch?
CURRENT_BRANCH=$(git symbolic-ref --short -q HEAD)
if [ "$CURRENT_BRANCH" = "development" ]; then
  echo "We are on development branch as expected"
else
  echo "You are on $CURRENT_BRANCH, I hope that is intended."
fi

# Don't pick the wrong credentials
export AWS_PROFILE=my-profile-name
# Are these credentials any good? TF checks late.
aws sts get-caller-identity
retVal=$?
if [ $retVal -ne 0 ]; then
   echo Current credentials are no good.
    kill -INT $$
    return
fi

# Are you even in the right workspace?
terraform workspace select dev

# now it is safe to plan.
terraform plan -out="plan.txt"
```

# Gitops for IAM

- Everything happens on server
- One developer runs verify & plan on a branch
- Another runs apply with secret credentials after merge request


- Assumes two developers
- Cycle time to change one line of code is very long
- Better for tweaking a known, working codebase



# Links

[Free training videos on Youtube for gitlab-ci.yml](https://www.youtube.com/playlist?list=PLcAljDQ0plzVU9qA39krLMS7pmijHb-16)
