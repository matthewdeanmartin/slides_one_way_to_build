# Goals

- Some mild advocacy.
Note: Build scripts are good and you should pay attention to them not existing or failing.

- Something you can use today.

# Problems you (may) have.

- You discover problems with your code late, possibly only when its in production.
- It takes forever to go from changing code to seeing it in production even after you ignore CAB and SDLC delays.

Build scripts find mistakes and automate most of the steps.



# Is it optional?



# Build Scripts are like governance

- Sometimes it is built in
  - you can't run code with bad datatypes in Java.
  - you can with python

- Code review is the most effective way to improve code quality
  - Linters do a better job of it than you
  - Code reformatters do an ever better job
  - Code review is now at a design level, not nitpick level


# Build Scripts are like doing work

- How else does a build and deploy happen?
- Is it optional?
- What should we skip?
  
Note: If a build script wasn't doing this work, someone would manually, just worse and less often.
It is boring, repetitious work. Is it optional? Skipping doing it doesn't always work well.
Skip anything that provides no value at all. Testing, error finding provide value and when a machine
is doing the work, even low value tasks can be done on every commit, not so on manual tasks.


# Build Scripts are like a testing team

- Build scripts find bugs everywhere
- Build scripts drop the cycle time from discovery to fix
- Complement to end-to-end testing, UAT, and sending it to prod to see if anyone complains 


# Build Scripts Elements


# Typical High Value Tools

 - Type checker 
 - Unit tests framework
 - Static code analysis
 - Dependency managers and packagers
Note: (sometimes called compilers, sometimes type annotations for TypeScript or Python)


# Getting there from here

Legacy code
 - Expect transition period, fail build based on lines of lint
 - Write any unit tests, even bad ones are better than none
 - Add typing to python and typescript incrementally
 - Expect to invest some time

Greenfield code
 - Use a good template if you can
 - Start with a build script first
 - Fail build on more than a handful of issues reported
 - Start strict, stay strict


# Solutions

build.sh
```bash
    pylint . && bandit . && mypy . && python setup.py sdist
```
Note: Trivial build scripts are bettern than none. Missing many concepts, in particular you can't
skip steps already done, no concept of change detection to trigger rebuilt/redo


makefile-like, e.g. maven, Makefile 
  - creates targets or output files when missing
  - targets have dependencies that need to be build once
  - skip if already build or if source didn't change


Dependency/Package centric
  - Pipfile
  - package.json
  - Dockerfile



# Build Servers

- Examples Gitlab's gitlab-ci.yml, Jenkinsfile (other exists such as circle ci, travis, github actions, Azure DevOps and many, many more)
- Gets work off your workstation
- Repeatablity
- Team visiblity into build status

```yaml
    build:
        script:
        - build.sh
```



# Challenge
### Getting Started
Creating the basic parts
- Use shared templates!
- CookieCutters for python projects


    
# Challenge
### Running tools locally
No good solution!
- All in one tools (mega linter)
- Consider running tools in Docker (link)
- Use precommit


# Challenge
### Slow builds
Use a base image (link) 


# Challenge
###Demonstrating to clients and management
Sonar
- Mediocre workflow, doesn't replace a build script
- Good for big picture reports 


Artifacts
- HTML reports
- Download, unzip, open
- Or, `aws s3 sync reports s3://reports_bucket`



# Packaging


# Deploying Packages

- Gitlab supports most package repositories
- AWS supports most package repositories too.


# Building Containers

- Docker in Docker for building
- Can "smoke test images"
- Gitlab docker repository
- AWS also has container repositories



# Flipping packages over the wall to server admins



# Continuous Deployment

- Spinning up machines
- Putting software on the machines to run
- Has become part of the build script now that "infrastructure is code"


# Provisioning AWS Infrastructure

- Use Terraform
- Spectacular complexity
- Unit of re-use is a module


# What is Terraform

- List of assets and properties.
- Compares to list of assets in AWS
- Makes changes to syncronize
- ... oh and it is a programming language too.
- ... and you will need hacks like null resources to run bash scripts



# Deploy Scripts 

- Gitlab natively works with Kubernetes
- If you use ECs, Gitlab has fewer built in features
- Your terraform scripts will get intertwined with your deploy scripts


# Interactive Terraform for non-IAM resourced
- Interactive plan and apply for everything except IAM

plan.sh
```bash
export AWS_PROFILE=my-profile-name
aws sts get-caller-identity
CURRENT_BRANCH=$(git symbolic-ref --short -q HEAD)
if [ "$CURRENT_BRANCH" = "development" ]; then
  terraform workspace select dev
  terraform apply "plan.txt"
else
  echo "Wrong branch."
fi
```

apply.sh
```bash
# Are you even on the right branch?
CURRENT_BRANCH=$(git symbolic-ref --short -q HEAD)
if [ "$CURRENT_BRANCH" = "development" ]; then
  export AWS_PROFILE=my-profile-name
  aws sts get-caller-identity
  terraform workspace select dev
  terraform plan -out="plan.txt"
else
  echo "Wrong branch."
fi
```

# Gitops for IAM

- Everything happens on server
- One developer runs verify & plan on a branch
- Another runs apply with secret credentials after merge request

- Assumes two developers
- Cycle time to change one line of code is very long
- Better for tweaking a known, working codebase



# Links

[Free training videos on Youtube for gitlab-ci.yml](https://www.youtube.com/playlist?list=PLcAljDQ0plzVU9qA39krLMS7pmijHb-16)
